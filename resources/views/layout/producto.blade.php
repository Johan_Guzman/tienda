@extends('layout.main')
<!--Titulo de Pagina-->
@section('titulo')
    <title>Inicio | Sport City</title>
@endsection
<!--Cerrar secion de "Usuario"-->
@section('sesionUs')
    Cerrar Sesion
@endsection
@section('contenido')
    @yield('migaja')
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- Product main img -->
					<div class="col-md-5 col-md-push-2">
						<div id="product-main-img">
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto1)}}" alt="Foto del Producto 1" width="640px" height="500px">
							</div>
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto2)}}" alt="Foto del Producto 2" width="640px" height="500px">
							</div>
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto3)}}" alt="Foto del Producto 3" width="640px" height="500px">
							</div>
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto4)}}" alt="Foto del Producto 4" width="640px" height="500px">
							</div>
                            <div class="product-preview">
                                <img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto5)}}" alt="Foto del Producto 5" width="640px" height="500px">
                            </div>
						</div>
					</div>
					<!-- /Product main img -->
					<!-- Product thumb imgs -->
					<div class="col-md-2  col-md-pull-5">
						<div id="product-imgs">
							<div class="product-preview">
                                <img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto1)}}" alt="Foto del Producto 1">
							</div>
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto2)}}" alt="Foto del Producto 2">
							</div>
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto3)}}" alt="Foto del Producto 3">
							</div>
							<div class="product-preview">
								<img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto4)}}" alt="Foto del Producto 4">
							</div>
                            <div class="product-preview">
                                <img src="{{asset('./img/'.$producto->categorias.'/'.$producto->foto5)}}" alt="Foto del Producto 5">
                            </div>
						</div>
					</div>
					<!-- /Product thumb imgs -->

					<!-- Product details -->
					<div class="col-md-5">
						<div class="product-details">
							<h2 class="product-name">{{$producto->nombre}}</h2>
							<div>
								<div class="product-rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-o"></i>
								</div>
							</div>
							<div>
                                @if($producto->precioEs == "0")
                                    <h3 class="product-price">${{$producto->precio}}.00</h3>
                                @else
                                    <h3 class="product-price">${{$producto->precioEs}}.00 <del class="product-old-price">
                                        ${{$producto->precio}}.00</del></h3>
                                @endif
								<span class="product-available">Disponible</span>
							</div>
							<p>{{$producto->descripcion}}</p>

							<div class="product-options">
								<label>
									Color
									<select class="input-select">
										<option value="0">{{$producto->color}}</option>
									</select>
								</label>
							</div>

							<div class="add-to-cart">
                                <br>
                                <!-- Modal para agregar producto -->
                                <button type="button" class="add-to-cart-btn" data-toggle="modal"
                                        data-target="#comprarProducto"><i class="fa fa-shopping-bag"></i>
                                    Comprar</button>
                                <!-- Modal para agregar producto -->
                                <button type="button" class="add-to-cart-btn" data-toggle="modal"
                                            data-target="#agregarCarrito"><i class="fa fa-shopping-cart"></i>
                                        Agregar a Carrito</button>
							</div>

							<ul class="product-btns">
								<li><a href="#"><i class="fa fa-exchange"></i> Comparar</a></li>
							</ul>

							<ul class="product-links">
								<li>Categoria:</li>
								<li><a href="#">{{$producto->categorias}}</a></li>
							</ul>

							<ul class="product-links">
								<li>Compartir:</li>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i></a></li>
							</ul>
                            <div>
                                <!-- Modal de Agregar a Carrito-->
                                <div class="modal fade" id="agregarCarrito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Agregar Producto "{{$producto->nombre}}"</h4>
                                            </div>
                                            <div class="modal-body">
                                               <div class="row">
                                                   <div class="col-md-3">
                                                       <img src="{{ asset('./img/'.$producto->categorias.'/'.$producto->foto1)}}" alt="" width="150px" height="150px">
                                                   </div>
                                                   <div class="col-md-8 row" id="datosProducto">
                                                       <form id="formCarrito" action="{{route('agregar.carrito')}}" method="post" class="carritoModal">
                                                           {{csrf_field()}}
                                                           <div class="row">
                                                               @if($producto->precioEs == "0")
                                                                    <div class="col-md-12">
                                                                       <div class="col-md-4">
                                                                           <label for="nombre" class="form-label">Articulo:</label>
                                                                           <input type="text" class="form-control sinborde" id="nombre" name="nombre" value="{{$producto->nombre}}"
                                                                                  readonly>
                                                                       </div>
                                                                       <div class="col-md-4">
                                                                           <label for="precio" class="form-label">Precio:</label>
                                                                           <input type="text" class="form-control" id="precio" name="precio" value="{{$producto->precio}}"
                                                                                  readonly>
                                                                       </div>
                                                                       <div class="col-md-4">
                                                                           <label for="color" class="form-label">Color:</label>
                                                                           <input type="text" class="form-control" id="color" name="color" value="{{$producto->color}}"
                                                                                  readonly>
                                                                       </div>
                                                                    </div>
                                                                   <div class="col-md-12">
                                                                       <div class="col-md-8 col-md-offset-2">
                                                                           <label for="cantidad" class="form-label">Cantidad:</label>
                                                                           <input type="text" class="form-control" id="cantidad" name="cantidad"
                                                                                  placeholder="Escriba un Numero" value="1" maxlength="3" onkeypress="return validaNumericos(event)">
                                                                       </div>
                                                                   </div>
                                                               @else
                                                                   <div class="col-md-12">
                                                                       <div class="col-md-4">
                                                                           <label for="nombre" class="form-label">Articulo:</label>
                                                                           <input type="text" class="form-control sinborde" id="nombre" name="nombre" value="{{$producto->nombre}}"
                                                                                  readonly>
                                                                       </div>
                                                                       <div class="col-md-4">
                                                                           <label for="precio" class="form-label">Precio Original:</label>
                                                                           <input type="text" class="form-control" id="precio" name="precio" value="{{$producto->precio}}"
                                                                                  readonly>
                                                                       </div>
                                                                       <div class="col-md-4">
                                                                           <label for="precioEs" class="form-label">Precio Especial:</label>
                                                                           <input type="text" class="form-control" id="precioEs" name="precioEs" value="{{$producto->precioEs}}"
                                                                                  readonly>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-md-12">
                                                                       <div class="col-md-4">
                                                                           <label for="color" class="form-label">Color:</label>
                                                                           <input type="text" class="form-control sinborde" id="color" name="color" value="{{$producto->color}}"
                                                                                  readonly>
                                                                       </div>
                                                                       <div class="col-md-8">
                                                                           <label for="cantidad" class="form-label">Cantidad:</label>
                                                                           <input type="text" class="form-control" id="cantidad" name="cantidad"
                                                                                  placeholder="Escriba un Numero" value="1" maxlength="3" onkeypress="return validaNumericos(event)">
                                                                       </div>
                                                                   </div>
                                                               @endif
                                                           </div>
                                                           <br>
                                                           <div class="modal-footer">
                                                               <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                               <button type="submit" class="btn btn-success" id="guardarCosasCarrito">Agregar a Carrito</button>
                                                           </div>
                                                       </form>
                                                   </div>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal de Comprar Producto-->
                                <div class="modal fade" id="comprarProducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Comprar Producto "{{$producto->nombre}}"</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <img src="{{ asset('./img/'.$producto->categorias.'/'.$producto->foto1)}}" alt="" width="150px" height="150px">
                                                    </div>
                                                    <div class="col-md-8 row" id="datosProducto">
                                                        <form id="formCarrito" action="{{route('comprar.producto')}}" method="post" class="carritoModal">
                                                            {{csrf_field()}}
                                                            <div class="row">
                                                                @if($producto->precioEs == "0")
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-4">
                                                                            <label for="nombre" class="form-label">Articulo:</label>
                                                                            <input type="text" class="form-control sinborde" id="nombre" name="nombre" value="{{$producto->nombre}}"
                                                                                   readonly>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label for="precio" class="form-label">Precio:</label>
                                                                            <input type="text" class="form-control" id="precio" name="precio" value="{{$producto->precio}}"
                                                                                   readonly>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label for="color" class="form-label">Color:</label>
                                                                            <input type="text" class="form-control" id="color" name="color" value="{{$producto->color}}"
                                                                                   readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-8 col-md-offset-2">
                                                                            <label for="cantidad" class="form-label">Cantidad:</label>
                                                                            <input type="text" class="form-control" id="cantidad" name="cantidad"
                                                                                   placeholder="Escriba un Numero" value="1" maxlength="3" onkeypress="return validaNumericos(event)">
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-4">
                                                                            <label for="nombre" class="form-label">Articulo:</label>
                                                                            <input type="text" class="form-control sinborde" id="nombre" name="nombre" value="{{$producto->nombre}}"
                                                                                   readonly>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label for="precio" class="form-label">Precio Original:</label>
                                                                            <input type="text" class="form-control" id="precio" name="precio" value="{{$producto->precio}}"
                                                                                   readonly>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label for="precioEs" class="form-label">Precio Especial:</label>
                                                                            <input type="text" class="form-control" id="precioEs" name="precioEs" value="{{$producto->precioEs}}"
                                                                                   readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-4">
                                                                            <label for="color" class="form-label">Color:</label>
                                                                            <input type="text" class="form-control sinborde" id="color" name="color" value="{{$producto->color}}"
                                                                                   readonly>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <label for="cantidad" class="form-label">Cantidad:</label>
                                                                            <input type="text" class="form-control" id="cantidad" name="cantidad"
                                                                                   placeholder="Escriba un Numero" value="1" maxlength="3" onkeypress="return validaNumericos(event)">
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <br>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                                <button type="submit" class="btn btn-success" id="guardarCosasCarrito">Comprar Producto</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- /Product details -->

					<!-- Product tab -->
					<div class="col-md-12">
						<div id="product-tab">
							<!-- product tab nav -->
							<ul class="tab-nav">
								<li class="active"><a data-toggle="tab" href="#tab1">Descripcion</a></li>
								<li><a data-toggle="tab" href="#tab2">Detalles</a></li>
								<li><a data-toggle="tab" href="#tab3">Opiniones (3)</a></li>
							</ul>
							<!-- /product tab nav -->

							<!-- product tab content -->
							<div class="tab-content">
								<!-- tab1  -->
								<div id="tab1" class="tab-pane fade in active">
									<div class="row">
										<div class="col-md-12">
											<p>{{$producto->descripcion}}</p>
										</div>
									</div>
								</div>
								<!-- /tab1  -->

								<!-- tab2  -->
								<div id="tab2" class="tab-pane fade in">
									<div class="row">
										<div class="col-md-12">
                                            <p>{{$producto->detalle}}</p>
										</div>
									</div>
								</div>
								<!-- /tab2  -->

								<!-- tab3  -->
								<div id="tab3" class="tab-pane fade in">
									<div class="row">
										<!-- Rating -->
										<div class="col-md-3">
											<div id="rating">
												<div class="rating-avg">
													<span>4.5</span>
													<div class="rating-stars">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-o"></i>
													</div>
												</div>
												<ul class="rating">
													<li>
														<div class="rating-stars">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
														</div>
														<div class="rating-progress">
															<div style="width: 80%;"></div>
														</div>
														<span class="sum">3</span>
													</li>
													<li>
														<div class="rating-stars">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-o"></i>
														</div>
														<div class="rating-progress">
															<div style="width: 60%;"></div>
														</div>
														<span class="sum">2</span>
													</li>
													<li>
														<div class="rating-stars">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
														</div>
														<div class="rating-progress">
															<div></div>
														</div>
														<span class="sum">0</span>
													</li>
													<li>
														<div class="rating-stars">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
														</div>
														<div class="rating-progress">
															<div></div>
														</div>
														<span class="sum">0</span>
													</li>
													<li>
														<div class="rating-stars">
															<i class="fa fa-star"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
															<i class="fa fa-star-o"></i>
														</div>
														<div class="rating-progress">
															<div></div>
														</div>
														<span class="sum">0</span>
													</li>
												</ul>
											</div>
										</div>
										<!-- /Rating -->

										<!-- Reviews -->
										<div class="col-md-6">
											<div id="reviews">
												<ul class="reviews">
													<li>
														<div class="review-heading">
															<h5 class="name">John</h5>
															<p class="date">27 DEC 2018, 8:0 PM</p>
															<div class="review-rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-o empty"></i>
															</div>
														</div>
														<div class="review-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
														</div>
													</li>
													<li>
														<div class="review-heading">
															<h5 class="name">John</h5>
															<p class="date">27 DEC 2018, 8:0 PM</p>
															<div class="review-rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-o empty"></i>
															</div>
														</div>
														<div class="review-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
														</div>
													</li>
													<li>
														<div class="review-heading">
															<h5 class="name">John</h5>
															<p class="date">27 DEC 2018, 8:0 PM</p>
															<div class="review-rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-o empty"></i>
															</div>
														</div>
														<div class="review-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
														</div>
													</li>
												</ul>
												<ul class="reviews-pagination">
													<li class="active">1</li>
													<li><a href="#">2</a></li>
													<li><a href="#">3</a></li>
													<li><a href="#">4</a></li>
													<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
												</ul>
											</div>
										</div>
										<!-- /Reviews -->

										<!-- Review Form -->
										<div class="col-md-3">
											<div id="review-form">
												<form class="review-form">
													<input class="input" type="text" placeholder="Tu nombre">
													<input class="input" type="email" placeholder="Tu Correo">
													<textarea class="input" placeholder="Tu Opinion"></textarea>
													<div class="input-rating">
														<span>Tu Calificacion: </span>
														<div class="stars">
															<input id="star5" name="rating" value="5" type="radio"><label for="star5"></label>
															<input id="star4" name="rating" value="4" type="radio"><label for="star4"></label>
															<input id="star3" name="rating" value="3" type="radio"><label for="star3"></label>
															<input id="star2" name="rating" value="2" type="radio"><label for="star2"></label>
															<input id="star1" name="rating" value="1" type="radio"><label for="star1"></label>
														</div>
													</div>
													<button class="primary-btn">Enviar</button>
												</form>
											</div>
										</div>
										<!-- /Review Form -->
									</div>
								</div>
								<!-- /tab3  -->
							</div>
							<!-- /product tab content  -->
						</div>
					</div>
					<!-- /product tab -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->
@endsection

@section('js')
    <script src="{{ asset('js/soloNumeros.js')}}"></script>
@endsection
