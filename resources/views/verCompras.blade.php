@extends('layout.main')
<!--Titulo de Pagina-->
@section('titulo')
    <title>Carrito | Sport City</title>
@endsection
<!--Cerrar secion de "Usuario"-->
@section('sesionUs')
    Cerrar Sesion
@endsection

@section('contenido')
    <div class="col-md-12 row titulo">
        <h1>Tus Compras </h1>
    </div>
    <div class="col-md-12 totalCarrito">
        <p>La referencia es, un numero unico que identifica si un producto se compro solo, o se compraron varios a la
            vez, si algun producto tiene una "unica" referencia significa que el producto se compro por separado, pero si
            mas de un producto tiene la misma referencia, significa que se compraron esos porductos "a la vez" por medio
            del carrito</p>
        <br>
    </div>
    <div class="container">
        <br><br>
        <div class="row">
            <!--Tabla de Agenda de Lunes -->
            <table class="table table-striped" style="width:100%" id="tableCarrito">
                <thead>
                <tr>
                    <th>Producto</th>
                    <th>Precio</th>
                    <th>Categoria</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th>Referencia</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@section('js')
    <!-- Ajax --> <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
    <!-- Jquery --> <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <!-- DataTables --> <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#tableCarrito').DataTable({
                "processing": true,
                "ajax": "{{ route('ver.las.compras')}}",
                "columns": [
                    {data: 'producto'},
                    {data: 'precio'},
                    {data: 'categorias'},
                    {data: 'cantidad'},
                    {data: 'total'},
                    {data: 'referencia'}
                ]
            });

        });
    </script>
@endsection
