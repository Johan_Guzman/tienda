@extends('layout.comprar')

@section('compras')
    <div class="order-col">
        <div>{{$cantidad}}x {{$producto->nombre}}</div>
        @if($producto->precioEs != "0")
            <div>${{$producto->precioEs}}.00</div>
        @else
            <div>${{$producto->precio}}.00</div>
        @endif
    </div>
@endsection
@section('totalcompra')
    @if(isset($total))
        <div><strong class="order-total">${{$total}}.00</strong></div>
    @endif
@endsection
@section('botonComprar')
    <a href="{{route('producto.comprado')}}" class="primary-btn order-submit">Comprar</a>
@endsection
