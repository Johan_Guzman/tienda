@extends('layout.main')
<!--Titulo de Pagina-->
@section('titulo')
    <title>Inicio | Sport City</title>
@endsection
<!--Cerrar secion de "Usuario"-->
@section('sesionUs')
    Cerrar Sesion
@endsection

@section('contenido')
    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- Fut Bol -->
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="{{asset('./img/futbol.png')}}" alt="Imagen sobre Deporte Fut Bol">
                        </div>
                        <div class="shop-body">
                            <h3>Deporte<br>Fut Bol</h3>
                            <a href="{{route('categoria.futbol')}}" class="cta-btn">Navegar
                                <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /Fut Bol -->

                <!-- Correr -->
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="{{asset('./img/correr.png')}}" alt="Imagen sobre Deporte Correr">
                        </div>
                        <div class="shop-body">
                            <h3>Deporte<br>Correr</h3>
                            <a href="{{route('categoria.correr')}}" class="cta-btn">Navegar <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /Correr -->

                <!-- Fitness -->
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img src="{{asset('./img/fitness.png')}}" alt="Imagen sobre Deporte Fintness">
                        </div>
                        <div class="shop-body">
                            <h3>Deporte<br>Fitness</h3>
                            <a href="{{route('categoria.fitness')}}" class="cta-btn">Navegar <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /Fitness -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- SECTION -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Lo Mas Nuevo</h3>
                        <div class="section-nav">
                            <ul class="section-tab-nav tab-nav">
                                <li class="active"><a data-toggle="tab" href="#nuevoDeportes">Deportes</a></li>
                                <li><a data-toggle="tab" href="#tab1">Dama</a></li>
                                <li><a data-toggle="tab" href="#tab1">Caballero</a></li>
                                <li><a data-toggle="tab" href="#tab1">Accesorios</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="nuevoDeportes" class="tab-pane active">
                                <div class="products-slick" data-nav="#slick-nav-1">
                                    <!-- product -->
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{asset('./img/balon.png')}}" alt="" height="280px">
                                            <div class="product-label">
                                                <span class="sale">-50%</span>
                                                <span class="new">Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">Fut Bol</p>
                                            <h3 class="product-name"><a href="#">Balon Adidas</a></h3>
                                            <h4 class="product-price">$500.00 <del class="product-old-price">$1,000.00</del></h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                                                <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Compartir</span></button>
                                                <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Detalles</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar a Carrito</button>
                                        </div>
                                    </div>
                                    <!-- /product -->
                                    <!-- product -->
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{asset('./img/guantes.png')}}" alt="" height="280px">
                                            <div class="product-label">
                                                <span class="sale">-30%</span>
                                                <span class="new">Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">Fut Bol</p>
                                            <h3 class="product-name"><a href="#">Guantes de Portero</a></h3>
                                            <h4 class="product-price">$350.00 <del class="product-old-price">$500.00</del></h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                                                <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Compartir</span></button>
                                                <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Detalles</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar a Carrito</button>
                                        </div>
                                    </div>
                                    <!-- /product -->
                                    <!-- product -->
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{asset('./img/tenis.png')}}" alt="" height="280px">
                                            <div class="product-label">
                                                <span class="sale">-10%</span>
                                                <span class="new">Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">Correr</p>
                                            <h3 class="product-name"><a href="#">Tenis Deportivos</a></h3>
                                            <h4 class="product-price">$900.00 <del class="product-old-price">$1,000.00</del></h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                                                <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Compartir</span></button>
                                                <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Detalles</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar a Carrito</button>
                                        </div>
                                    </div>
                                    <!-- /product -->
                                    <!-- product -->
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{asset('./img/reloj.png')}}" alt="" height="280px">
                                            <div class="product-label">
                                                <span class="sale">-20%</span>
                                                <span class="new">Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">Correr</p>
                                            <h3 class="product-name"><a href="#">Monitor Inteligente</a></h3>
                                            <h4 class="product-price">$640.00 <del class="product-old-price">$800.00</del></h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                                                <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Compartir</span></button>
                                                <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Detalles</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar a Carrito</button>
                                        </div>
                                    </div>
                                    <!-- /product -->
                                    <!-- product -->
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{asset('./img/short.png')}}" alt="" height="280px">
                                            <div class="product-label">
                                                <span class="sale">-25%</span>
                                                <span class="new">Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">Fitness</p>
                                            <h3 class="product-name"><a href="#">Short Deportivo</a></h3>
                                            <h4 class="product-price">$262.5 <del class="product-old-price">$350.00</del></h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                                                <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Compartir</span></button>
                                                <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Detalles</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar a Carrito</button>
                                        </div>
                                    </div>
                                    <!-- /product -->
                                    <!-- product -->
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="{{asset('./img/cubrebocas.png')}}" alt="" height="280px">
                                            <div class="product-label">
                                                <span class="sale">-80%</span>
                                                <span class="new">Nuevo</span>
                                            </div>
                                        </div>
                                        <div class="product-body">
                                            <p class="product-category">Fitness</p>
                                            <h3 class="product-name"><a href="#">Cubrebocas de Tela</a></h3>
                                            <h4 class="product-price">$60.00 <del class="product-old-price">$300.00</del></h4>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="product-btns">
                                                <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Agregar a Favoritos</span></button>
                                                <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Compartir</span></button>
                                                <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Detalles</span></button>
                                            </div>
                                        </div>
                                        <div class="add-to-cart">
                                            <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> Agregar a Carrito</button>
                                        </div>
                                    </div>
                                    <!-- /product -->
                                </div>
                                <div id="slick-nav-1" class="products-slick-nav"></div>
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- Products tab & slick -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- HOT DEAL SECTION -->
    <div id="hot-deal" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="hot-deal">
                        <ul class="hot-deal-countdown">
                            <li>
                                <div>
                                    <h3>02</h3>
                                    <span>Days</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>10</h3>
                                    <span>Hours</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>34</h3>
                                    <span>Mins</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>60</h3>
                                    <span>Secs</span>
                                </div>
                            </li>
                        </ul>
                        <h2 class="text-uppercase">Sport Hot Sale</h2>
                        <p>Las mejores ofertas con hasta 50% de descuento</p>
                        <a class="primary-btn cta-btn" href="#">Conocer Mas</a>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /HOT DEAL SECTION -->
@endsection
