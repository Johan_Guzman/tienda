<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Registro</title>
    <!-- CSS -->
    <!-- Bootstrap --> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Ajax --> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />

    <link rel="stylesheet" href="{{ asset('css/registro.css')}}">
</head>
<body>
<div class="container">
    <div class="row text-center login-page">
        <div class="col-md-12 login-form">
            <form action="{{route('registrar.usuario')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row present">
                    <div class="col-md-12">
                        <p class="login-form-font-header"><span>Sport City</span><p>
                        <p class="login-form-font-header2">Registrate para tener cuenta en Sport City<p>
                    </div>
                    <label class="text-danger">
                        @if(isset($estatus))
                            <label class="text-danger">{{$mensaje}}</label>
                        @endif
                    </label>
                </div>
                <div class="row">
                    <div class="col-md-8 offset-md-2 row">
                        <div class="col-md-4">
                            <label for="nombre" class="form-label">Nombre:<span>*</span></label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" maxlength="20">
                        </div>
                        <div class="col-md-4">
                            <label for="aPaterno" class="form-label">Apellido Paterno:<span>*</span></label>
                            <input type="text" class="form-control" id="aPaterno" name="aPaterno" placeholder="Apellido" maxlength="20">
                        </div>
                        <div class="col-md-4">
                            <label for="aMaterno" class="form-label">Apellido Materno:<span>*</span></label>
                            <input type="text" class="form-control" id="aMaterno" name="aMaterno" placeholder="Apellido" maxlength="20">
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-2 row">
                        <div class="col-md-6">
                            <label for="correo" class="form-label">Correo:<span>*</span></label>
                            <input type="email" class="form-control" id="correo" name="correo" placeholder="alguien@example.com" maxlength="70">
                        </div>
                        <div class="col-md-6">
                            <label for="confcorreo" class="form-label">Confirmar Correo:<span>*</span></label>
                            <input type="email" class="form-control" id="confcorreo" name="confcorreo" placeholder="alguien@example.com" maxlength="70">
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-2 row">
                        <div class="col-md-6">
                            <label for="password" class="form-label">Contraseña:<span>*</span></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="********" maxlength="255">
                        </div>
                        <div class="col-md-6">
                            <label for="confpassword" class="form-label">Confirmar Contraseña:<span>*</span></label>
                            <input type="password" class="form-control" id="confpassword" name="confpassword" placeholder="********" maxlength="255">
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-2 row">
                        <div class="col-md-6 offset-md-3 row" id="enviar">
                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Crear Cuenta">
                        </div>
                    </div>
                </div>
            </form>
            <div class="text-center" id="login">
                <a class="small" href="{{route('login')}}">¿Tienes una cuenta? ¡Inicia Sesión!</a>
            </div>
        </div>
    </div>
</div>

<!-- JS -->
<!-- Jquery --> <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
<!-- Ajax --> <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
</body>
</html>
