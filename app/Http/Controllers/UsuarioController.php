<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use App\Models\Productos;
use App\Models\Carrito;
use App\Models\Compras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class UsuarioController extends Controller
{
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL INDEX
    public function index(){
        return view("index");
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL LOGIN
    public function login(){
        return view("login");
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL REGISTRO
    public function registro(){
        return view("registro");
    }
        // ------------------------------------------------------------- FUNCION PARA REGISTRAR USUARIO
    public function registroValidar(Request $informacion){
        //Valida que se hayan llenado todos los datos
        if(!$informacion->nombre || !$informacion->aPaterno || !$informacion->aMaterno || !$informacion->correo ||
            !$informacion->confcorreo || !$informacion->password || !$informacion->confpassword){
            return view("registro",["estatus"=> "error", "mensaje"=> "¡Complete todos los campos Obligatorios!"]);
        }
        //Valida que el correo no exista en la base de datos
        $usuario = Usuario::where('correo',$informacion->email)->first();
        if($usuario == true){
            return view("registro",["estatus"=> "error", "mensaje"=> "Ya existe una cuenta con el Correo Ingresado"]);
        }
        //Se extraen todas las variables del formulario
        $nombre = $informacion->nombre;
        $aPaterno = $informacion->aPaterno;
        $aMaterno = $informacion->aMaterno;
        $correo = $informacion->correo;
        $confcorreo = $informacion->confcorreo;
        $password = $informacion->password;
        $confpassword = $informacion->confpassword;
        //Se valida que el correo sea el mismo (confirmacion)
        if(($correo != $confcorreo)){
            return view("registro",["estatus"=> "error", "mensaje"=> "Los Correos No Coinciden"]);
        }
        //Se valida que la contraseña sea el mismo (confirmacion)
        if(($password != $confpassword)){
            return view("registro",["estatus"=> "error", "mensaje"=> "Las Contraseñas no Coinciden"]);
        }
        //Se sube la informacion a DB
        $usuario = new Usuario();
        $usuario->nombre =  $nombre;
        $usuario->aPaterno = $aPaterno;
        $usuario->aMaterno = $aMaterno;
        $usuario->correo =  $correo;
        $usuario->password = bcrypt($password);
        $usuario->save();
        return view("login",["estatus"=> "aprobado", "mensaje"=> "¡Cuenta Creada!"]);
    }
    // ------------------------------------------------------------- FUNCION PARA VALIDAR LOGIN
    public function iniciarSecion(Request $informacion){
        //Valida que se escriba el correo y la contraseña
        if(!$informacion->correo || !$informacion->password){
            return view("login",["estatus"=> "error", "mensaje"=> "¡Completa los campos!"]);
        }
        //Busca que el Correo este registrado
        $usuario = Usuario::where("correo",$informacion->correo)->first();
        if(!$usuario){
            return view("login",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);
        }
        //Verifica la Contraseña del Uusario
        if(!Hash::check($informacion->password,$usuario->password)){
            return view("login",["estatus"=> "error", "mensaje"=> "¡Contraseña Incorrecta!"]);
        }
        //Valida que se inicioe Secion
        Session::put('usuario',$usuario);
        //Creamos la Variable de Secion dentro para usar los datos
        session_start();
        $_SESSION['perfil'] = $informacion->correo;
        //Redireccionamos a la Pagina Principal
        return redirect()->route('usuario.inicio');
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL INICIO

    public function inicio(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        return view("inicio", ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras], compact('perfil'));

    }
    // ------------------------------------------------------------- FUNCION PARA CERRAR SESION
    public function cerrarSesion(){
        if(Session::has('usuario'))
            Session::forget('usuario');
        return redirect()->route('index');
    }

    public function verProductoFutBol($id){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        $producto = Productos::where("id", $id)->where("categorias", "futbol")->first();
        return view('verProducto', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras], compact('producto'));
    }
    public function verProductoCorrer($id){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        $producto = Productos::where("id", $id)->where("categorias", "correr")->first();

        return view('verProducto', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras], compact('producto'));
    }
    public function verProductoFitness($id){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        $producto = Productos::where("id", $id)->where("categorias", "fitness")->first();

        return view('verProducto', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras], compact('producto'));
    }

    public function futbol(){
        $productos = Productos::where("categorias", "futbol")->get();
        $categoria = Productos::select("categorias")->where("categorias", "futbol")->first();
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        if($categoria == null){
            return view('futbol', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
                $cosasCarrito, "cosasCompras" => $cosasCompras], compact('productos'));
        }else{
            return view('futbol', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
                $cosasCarrito, "cosasCompras" => $cosasCompras], compact('productos'),
                compact('categoria'));
        }
    }
    public function correr(){
        $productos = Productos::where("categorias", "correr")->get();
        $categoria = Productos::select("categorias")->where("categorias", "correr")->first();
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        if($categoria == null){
            return view('correr', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
                $cosasCarrito, "cosasCompras" => $cosasCompras], compact('productos'));
        }else{

            return view('correr', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
                $cosasCarrito, "cosasCompras" => $cosasCompras], compact('productos'), compact('categoria'));
        }
    }
    public function fitness(){
        $productos = Productos::where("categorias", "fitness")->get();
        $categoria = Productos::select("categorias")->where("categorias", "fitness")->first();
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        if($categoria == null){
            return view('fitness', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
                $cosasCarrito, "cosasCompras" => $cosasCompras], compact('productos'));
        }else{

            return view('fitness', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
                $cosasCarrito, "cosasCompras" => $cosasCompras], compact('productos'), compact('categoria'));
        }
    }

    public function agregarCarrito(Request $datos){
        $nombre = $datos->nombre;
        $precio1 = $datos->precio;
        if($datos->precioEs != null){
            $precio = $datos->precioEs;
        }else{
            $precio = $precio1;
        }
        $color = $datos->color;
        $cantidad = $datos->cantidad;
        session_start();
        $usuario = Usuario::where("correo",$_SESSION['perfil'])->first();
        $producto = Productos::where("nombre", $nombre)->where("precio", $precio1)->where("color",$color)
            ->first();
        $total = $precio * $cantidad;
        $carrito = new Carrito();
        $carrito->usuario = $usuario->correo;
        $carrito->foto = $producto->foto1;
        $carrito->producto = $producto->nombre;
        if($producto->precioEs == "0"){
            $carrito->precio = $producto->precio;
        }else{
            $carrito->precio = $producto->precioEs;
        }
        $carrito->categorias = $producto->categorias;
        $carrito->total = $total;
        $carrito->cantidad = $cantidad;
        $carrito->save();
        return back();
    }
    public function verCarrito(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        $total = null;
        foreach ($cosasCarrito as $micarrito){
            $total = $total + $micarrito->total;
        }
        return view('verCarrito', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras, "totalCarrito" => $total]);
    }
    public function carritoVer(Request $request){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        if($request->ajax()){
            $carritoUsuario = Carrito::select("foto", "producto", "precio", "categorias", "cantidad", "total")
                ->where("usuario", $perfil->correo)->get();
            return DataTables::of($carritoUsuario)
                ->make(true);
        }
        return redirect()->route('ver.mi.carrito');
    }

    public function compraCarrito(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        $total = null;
        foreach ($cosasCarrito as $micarrito){
            $total = $total + $micarrito->total;
        }
        return view('comprarCarrito', ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras, "totalCarrito" => $total]);
    }
    public function carritoComprado(){
        session_start();
        $usuario = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$usuario->correo)->get();
        $carrito = Carrito::where("usuario",$usuario->correo)->count();
        $cosasCompras = Compras::where("usuario",$usuario->correo)->get();
        $compras = Compras::where("usuario",$usuario->correo)->count();
        $total = null;
        foreach ($cosasCarrito as $micarrito){
            $total = $total + $micarrito->total;
        }
        $referencia = null;
        $letras = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z');
        $letrasM = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z');
        $numeros = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
        do{
            for($x = 0; $x<10; $x++){
                $referencia = $referencia.$letras[array_rand($letras)];//Asignamos una letra minuscula al azar
                $referencia = $referencia.$letrasM[array_rand($letrasM)];//Asignamos una letra mayuscula al azar
                $referencia = $referencia.$numeros[array_rand($numeros)];//Asignamos un numero al azar
            }
        }while($referencia == Compras::where('referencia',$referencia)->first());

        foreach ($cosasCarrito as $carritoComprado){
            $comprar = new Compras();
            $comprar->usuario = $usuario->correo;
            $comprar->producto = $carritoComprado->producto;
            $comprar->precio = $carritoComprado->precio;
            $comprar->categorias = $carritoComprado->categorias;
            $comprar->total = $carritoComprado->total;
            $comprar->referencia = $referencia;
            $comprar->cantidad = $carritoComprado->cantidad;
            $comprar->save();
        }
        $x = 0;
        while($x < $carrito){
            $finCarrito = Carrito::where("usuario", $usuario->correo)->first();
            $finCarrito->delete();
            $x++;
        }
        return redirect()->route('compras.hechas');
    }
    public function compraProducto(Request $datos){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        $nombre = $datos->nombre;
        $precio1 = $datos->precio;
        if($datos->precioEs != null){
            $precio = $datos->precioEs;
        }else{
            $precio = $precio1;
        }
        $color = $datos->color;
        $cantidad = $datos->cantidad;
        $producto = Productos::where("nombre", $nombre)->where("precio", $precio1)->where("color",$color)
            ->first();
        $total = $precio * $cantidad;
        $_SESSION['compraProductoMomento'] = $producto->id;
        $_SESSION['cantidadProducto'] = $cantidad;
        return view("comprarProducto", ["admin"=> "usuario", "producto"=> $producto, "cantidad"=> $cantidad,
            "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" => $cosasCarrito, "total"=> $total,
            "cosasCompras" => $cosasCompras], compact('perfil'));
    }
    public function productoComprado(){
        session_start();
        $usuario = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$usuario->correo)->get();
        $carrito = Carrito::where("usuario",$usuario->correo)->count();
        $cosasCompras = Compras::where("usuario",$usuario->correo)->get();
        $compras = Compras::where("usuario",$usuario->correo)->count();
        $productoMomento = $_SESSION['compraProductoMomento'];
        $cantidadMomento = $_SESSION['cantidadProducto'];
        $producto = Productos::where("id", $productoMomento)->first();
        $total = $producto->precio * $cantidadMomento;
        $referencia = null;
        $letras = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z');
        $letrasM = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z');
        $numeros = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
        do{
            for($x = 0; $x<10; $x++){
                $referencia = $referencia.$letras[array_rand($letras)];//Asignamos una letra minuscula al azar
                $referencia = $referencia.$letrasM[array_rand($letrasM)];//Asignamos una letra mayuscula al azar
                $referencia = $referencia.$numeros[array_rand($numeros)];//Asignamos un numero al azar
            }
        }while($referencia == Compras::where('referencia',$referencia)->first());
            $comprar = new Compras();
            $comprar->usuario = $usuario->correo;
            $comprar->producto = $producto->nombre;
            $comprar->precio = $producto->precio;
            $comprar->categorias = $producto->categorias;
            $comprar->total = $total;
            $comprar->referencia = $referencia;
            $comprar->cantidad = $cantidadMomento;
            $comprar->save();
        return redirect()->route('compras.hechas');
    }
    public function compras(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $cosasCarrito = Carrito::where("usuario",$perfil->correo)->get();
        $carrito = Carrito::where("usuario",$perfil->correo)->count();
        $cosasCompras = Compras::where("usuario",$perfil->correo)->get();
        $compras = Compras::where("usuario",$perfil->correo)->count();
        return view("verCompras", ["admin"=> "usuario", "carrito"=> $carrito, "compras"=> $compras, "cosasCarrito" =>
            $cosasCarrito, "cosasCompras" => $cosasCompras], compact('perfil'));
    }
    public function comprasVer(Request $request){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        if($request->ajax()){
            $comprasUsuario = Compras::select("producto", "precio", "categorias", "cantidad", "total",
                        "referencia")->where("usuario", $perfil->correo)->get();
            return DataTables::of($comprasUsuario)
                ->make(true);
        }
        return redirect()->route('compras.hechas');
    }
}
