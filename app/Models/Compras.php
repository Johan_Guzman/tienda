<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compras extends Model
{
    protected $table = "compras";
    protected $fillable=["usuario", "foto", "producto", "precio", "categorias", "total", "referencia", "cantidad"];
}
