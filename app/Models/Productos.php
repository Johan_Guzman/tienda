<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = "producto";
    protected $fillable=["nombre", "precio", "descuento", "precioEs", "descripcion", "color", "categorias", "detalle",
     "foto1", "foto2", "foto3", "foto4", "foto5"];
}
