<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',30);
            $table->string('precio', 30);
            $table->string('descuento', 5);
            $table->string('precioEs',30)->nullable();
            $table->string('descripcion', 100);
            $table->string('color', 20);
            $table->string('categorias', 30);
            $table->string('detalle', 100);
            $table->string('ruta', 30);
            $table->string('foto1', 60);
            $table->string('foto2', 60);
            $table->string('foto3', 60);
            $table->string('foto4', 60);
            $table->string('foto5', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}
